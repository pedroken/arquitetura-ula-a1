package ula.pedro;
public class Registrador implements Cloneable {
	
	public static final int QTDBITS = 8;
	private String  valor;
	
	public Registrador() {
		
	}
	

	public Registrador(String valor) {
		this.valor = completaBits(valor);;
	}
	

	public Registrador(int valor) {
		this.valor = this.getValorBinario(valor);
	}
	
	public String getValorBinario() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = completaBits(valor);;
	}
	
	public void setValor(int valor) {
		this.valor = this.getValorBinario(valor);
	}

	private String getValorBinario( int dec ) {  
		String result = "";  

		while( dec > 0 ) {  
			result = (dec & 1) + result;  
			dec >>= 1;  
		}  
		return completaBits(result);
	}  


	public int getValorDecimal() {  
		int i, result = 0;  

		for( i = 0; i < this.valor.length(); i++ ) {  
			result <<= 1;  
			if( this.valor.charAt( i ) == '1' ) result++;  
		}  

		return result;  
	}  

	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	private static String completaBits(String bitsACompletar){
		while(bitsACompletar.length() < Registrador.QTDBITS){
			bitsACompletar = "0" + bitsACompletar;
		}
		return bitsACompletar;
	}
}