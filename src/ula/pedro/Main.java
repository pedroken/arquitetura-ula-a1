package ula.pedro;

import java.util.Scanner;
import static java.lang.Math.random;;

public class Main {
	static Registrador registA;
	static Registrador registB;
	static ULA ula;

	public static void main(String[] args) {
		Integer  selecao 	= 0;
		String  operacao= "0";
		String  valor	= "0";
		
		registA = new Registrador((int)(random() * 255));
		registB = new Registrador((int)(random() * 255));
		ula = new ULA();

		Scanner input = new Scanner(System.in);
		printMenu();

		while(selecao != 8){


			try {

				Integer opcaoTemp = pegarValorDigitado();
				if(opcaoTemp != null){
					selecao = opcaoTemp;
				}
				
				switch (selecao) {
				case 1:
					System.out.print("Digite o valor do registrador A :");
					valor = input.next();
					registA.setValor(valor);

					limparConsole();
					printMenu();
					break;

				case 2:
					System.out.print("Digite o valor do registrador B :");
					valor = input.next();
					registB.setValor(valor);

					limparConsole();
					printMenu();
					break;

				case 3:
					System.out.print("Valor do registrador A : "+ registA.getValorBinario());
					printMenu();
					break;

				case 4:
					System.out.print("Valor do registrador B : "+ registB.getValorBinario());
					printMenu();
					break;

				case 5:
					System.out.print("\nRegistrador de flags: ");
					for(int i = 0 ; i < ula.getRegistradorDeFlags().length ; i++){
						System.out.print(ula.getRegistradorDeFlags(i) + " ");
					}
					printMenu();
					break;

				case 6:
					operacao = "0";
					imprimirMenuOperacoes();

					while (true){

						operacao = input.next();
						if(ula.possuiOperacao(operacao)){
							System.out.println("\nOperação "+ operacao + " escolida.");
							printMenu();
							break;

						}else{

							limparConsole();
							System.out.println("\nOperação invalida. Tente Novamente: ");
						}

					}
					break;

				case 7:
					if (operacao.equals("3")){
						System.out.printf("\n%S",ula.operaracaoDiv(registA, registB));
						
					}else if (operacao.equals("4")){
						System.out.printf("\n%S",ula.operaracaoMult(registA, registB));}
						else if (operacao.equals("2")){
							
							System.out.printf("\n%S",ula.operacaoSubtracaoAMenosB(registA, registB));
						
					} else{
					registA.setValor(ula.executar(operacao, registA, registB));
					System.out.printf("%s",registA.getValorBinario());}
					break;

				case 8:
					System.out.println("Terminando execução...");
					System.exit(0);
					break;

				default:
					System.out.print("Opção Invalida, Tente novamente.\nEscolha uma opção: ");
					break;
				}	

			} catch (Exception e) {
				System.out.println("Ocorreu um erro: " + e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public static void printMenu(){


		String menu  = "\n\n";
		menu = menu + "1. Definir registrador A	 	\n";
		menu = menu + "2. Definir registrador B 	\n";
		menu = menu + "3. Ler registrador A (Acc)  	\n";
		menu = menu + "4. Ler registrador B 		\n";
		menu = menu + "5. Ler registrador de flags 	\n";
		menu = menu + "6. Definir operação 		 	\n";
		menu = menu + "7. Executar ULA 			 	\n";
		menu = menu + "8. Sair					 	\n";
		menu = menu + "\nEscolha uma opçãoo: ";	

		System.out.print(menu);
	}

	public static void imprimirMenuOperacoes (){

		String menu = "\n\n";
		menu = menu + "1 - Soma	 		\n";
		menu = menu + "2 - Subtracao		\n";
		menu = menu + "3 - Divisao		\n";
		menu = menu + "4 - Multiplicacao		\n";
		
		menu = menu + "Digite o numero da operacao:\n";
		System.out.print(menu);
	}

	public static void limparConsole(){

		for(int i= 0 ; i<50 ; i++ ){
			System.out.println(" ");
		}
	}

	private static Integer pegarValorDigitado(){
		Integer integer = null;
		Scanner input = new Scanner(System.in);
		
		try {

			String teclado = input.next();
			integer = Integer.valueOf(teclado) ; 
			
		} catch (NumberFormatException e) {
			integer = null;
		}
		return integer;
	}
}