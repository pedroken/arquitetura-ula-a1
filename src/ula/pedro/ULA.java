package ula.pedro;

public class ULA {

	private String[] registradorDeFlags;
	private String[] operacoes; 
	private Registrador resultado;

	/**
	 * Instancia uma ULA padrão de 16 operações
	 */
	public ULA() {
		this.resultado = new Registrador();
		this.operacoes = new String[]{"0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
		this.registradorDeFlags = new String[]{"-","-","-","-"};
	}

	public String[] getRegistradorDeFlags() {
		return registradorDeFlags;
	}

	public String getRegistradorDeFlags(int index) {
		return registradorDeFlags[index];
	}
	
	public void setRegistradorDeFlags(String[] registradorDeFlags) {
		this.registradorDeFlags = registradorDeFlags;
	}

	public void setRegistradorDeFlags(int index, String valor) {
		this.registradorDeFlags[index] = valor;
	}

	public String getResultado() {
		return resultado.getValorBinario();
	}

	public boolean possuiOperacao(String operacao){

		boolean possui = false;
		int tempOperacao = binaryToDec(operacao);
		
		for(int i = 0; i < this.operacoes.length ; i++){
			if (binaryToDec(this.operacoes[i]) == tempOperacao){
				possui = true;
				break;
			}
		}
		return possui;
	}

	public String executar(String operacao, Registrador registradorA, Registrador registradorB){

		
		Registrador aTemp = null;
		Registrador bTemp = null;
		
		try {
		
			aTemp = (Registrador) registradorA.clone();
			bTemp = (Registrador) registradorB.clone();
		
		} catch (CloneNotSupportedException e) {
			aTemp = registradorA;
			bTemp = registradorB;
		}
	
	
		String tempOperacao = String.valueOf(binaryToDec(operacao));
		
		if(tempOperacao.equalsIgnoreCase("1")){
			resultado.setValor(operacaoSoma( aTemp, bTemp));
		}
	
		if(tempOperacao.equalsIgnoreCase("2")){
			resultado.setValor(operacaoSubtracaoAMenosB(aTemp, bTemp));
		}
		
			if(tempOperacao.equalsIgnoreCase("3")){
				resultado.setValor(operaracaoDiv(aTemp, bTemp));
				
				
			}
			
		
			if(tempOperacao.equalsIgnoreCase("4")){
				resultado.setValor(operaracaoMult(aTemp, bTemp));
			}
	


		return resultado.getValorBinario();

	}

	/**
	 * Executa operação soma
	 * @param registradorA
	 * @param registradorB
	 * @return
	 */
	protected String operacaoSoma(Registrador registradorA, Registrador registradorB){
		Registrador resultado = new Registrador();
		resultado.setValor(registradorA.getValorDecimal() + registradorB.getValorDecimal());
		
		if(resultado.getValorDecimal() > 255 ){
			resultado.setValor(0);
			setRegistradorDeFlags(0, "1");
		}
		
		
		return resultado.getValorBinario();
	}
	

		/**
	 * Executa operação subtração.
	 * @param registradorA
	 * @param registradorB
	 * @return
	 */
	protected String operacaoSubtracaoAMenosB(Registrador registradorA, Registrador registradorB){
		int result = registradorA.getValorDecimal() - registradorB.getValorDecimal();
		if(result < 0){
			resultado.setValor(0);
			setRegistradorDeFlags(1, "1");
		}else{
			resultado.setValor(result);
		}
		return resultado.getValorBinario();
	}

	
	
	protected String operaracaoDiv(Registrador registradorA, Registrador registradorB){
		int result = (int) (registradorA.getValorDecimal() / registradorB.getValorDecimal());
		//	setRegistradorDeFlags(0, "0");
			resultado.setValor(result);
	
		return resultado.getValorBinario();
	}


	
	protected String operaracaoMult(Registrador registradorA, Registrador registradorB){
		int result = (int) (registradorA.getValorDecimal() * registradorB.getValorDecimal());
		//	setRegistradorDeFlags(0, "0");
			resultado.setValor(result);
	
		return resultado.getValorBinario();
	}



	/** 
	 * Converts binary to decimal 
	 * 
	 * @param bin The binary number to convert 
	 * @return An integer containing the decimal number. 
	 */  
	private static int binaryToDec( String bin ) {  
		int i, result = 0;  

		for( i = 0; i < bin.length(); i++ ) {  
			result <<= 1;  
			if( bin.charAt( i ) == '1' ) result++;  
		}  

		return result;  
	}  
}


